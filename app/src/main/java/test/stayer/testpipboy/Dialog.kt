package test.stayer.testpipboy

import android.app.DialogFragment
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog.*
import kotlinx.android.synthetic.main.dialog.view.*


/**
 * Created by stayer on 2/20/18.
 */
class Dialog: DialogFragment(), View.OnClickListener {

    var newBtnName = ""

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.setTitle("Test Dialog Fragment")
        val v = inflater!!.inflate(R.layout.dialog, null)
        v.btnDialogOK.setOnClickListener(this)
        return v
    }

    override fun onClick(view: View) {
        when (view) {
            btnDialogOK -> {
                newBtnName = editTextBtnName.text.toString()
                activity.btnActivateDialog.text = newBtnName
                dismiss()
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        Log.d(MainActivity.LOG_TAG, "Dialog: OnDismiss")
    }

    override fun onCancel(dialog: DialogInterface?) {
        super.onCancel(dialog)
        Log.d(MainActivity.LOG_TAG, "Dialog: OnCancel")
    }
}