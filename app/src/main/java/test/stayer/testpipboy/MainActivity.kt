package test.stayer.testpipboy

import android.app.DialogFragment
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var dialog: Dialog

    companion object {
        const val LOG_TAG = "test"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dialog = Dialog()

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)

        btnActivateDialog.setOnLongClickListener({ view ->
            dialog.show(fragmentManager, "fuck")
            true
        })
    }

    fun onClickBtn(view: View) {
        when (view) {
            btnChangeMapType -> {
                when (mMap.mapType) {
                    GoogleMap.MAP_TYPE_SATELLITE -> {
                        mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
                        tvMapType.text = getString(R.string.hybrid)
                    }
                    GoogleMap.MAP_TYPE_HYBRID -> {
                        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                        tvMapType.text = getString(R.string.normal)
                    }
                    GoogleMap.MAP_TYPE_NORMAL -> {
                        mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
                        tvMapType.text = getString(R.string.satellite)
                    }
                }
            }

            btnRandom -> {
                val iRand = Random().nextInt(10)
                Toast.makeText(
                        this,
                        "Add random count: $iRand",
                        Toast.LENGTH_SHORT).show()
                tvCentral.text = solveCount(iRand).toString()
            }

            btnActivateDialog -> {
                Toast.makeText(this, "Try to held button", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun solveCount(i: Int): Int {
        val sCount = tvCentral.text.toString()
        var count = Integer.parseInt(sCount)
        count += i
        return count
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

// Add a marker in Sydney and move the camera
        mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
        val home = LatLng(55.970367, 92.883547)
        mMap.addMarker(MarkerOptions().position(home).title("Marker in Home"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(home, 16f))
    }
}
